// Faça um programa que calcule a media das quatro motas de um aluno e informa se ele foi aprovado ou reprovado. A nota de um corte é de 6 pontos.

namespace exercicio_1
{
    let nota1, nota2, nota3, nota4, media: number;

    nota1 = 10;
    nota2 = 6;
    nota3 = 4;
    nota4 = 9;

    media = (nota1 + nota2 + nota3 + nota4) / 4;
    
    if(media > 6)

    {console.log("Aluno aprovado!");}

    else
    {console.log("Aluno reprovado!");}

}
