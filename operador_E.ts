//Testando o operador E- &&

namespace operador_E

{
    /*
    let possuiCartão:boolean;
    verdadeiro ou falso
    */

    let idade = 18; 
    let maiorIdade = idade >= 18; 
    let possuiCarteiraDeMotorista = true;  
    let podedirigir = maiorIdade && possuiCarteiraDeMotorista; 
    
    console.log(podedirigir); // true
}