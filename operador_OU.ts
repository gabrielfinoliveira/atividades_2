// Testando o operador OU -||
namespace operador_OU.Ts
{
    let idade = 16;
    let maiorIdade = idade > 18;
    let possuiAutorizacaoDosPais = true;

    let podeBeber = maiorIdade ||
    possuiAutorizacaoDosPais;

    console.log(podeBeber); // true

}