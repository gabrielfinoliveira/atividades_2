namespace atividade_horas
// Crie um algoritmo que solicite o nome de um usuário e exiba uma mensagem de boas-vindas personalizada de acordo com o horário do dia (bom dia, boa tarde ou boa noite).
{
    const nome_de_usuario = "Gabriel Finzetto"
    const data = new Date();
    const horas = data.getHours();
    
    if (horas > 1 && horas <= 12)
    {
        console.log(`Bom dia ${nome_de_usuario}`);
    }

    else if (horas > 12 && horas <= 18)
    {
        console.log(`Boa tarde ${nome_de_usuario}`);
    }

    else if (horas > 18 && horas <= 23)
    {
        console.log(`Boa noite ${nome_de_usuario}`);
    }
}