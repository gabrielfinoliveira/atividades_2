namespace atividade_7.ts
//  Faça um programa que leia um número inteiro entre 1 e 12 e imprima o mês correspondente, sendo 1 para janeiro, 2 para fevereiro, e assim por diante.

{
    let mês: number;
    mês = 5
    
    if (mês == 1)
    {
        console.log("Janeiro");
    }
    
    else if (mês == 2)
    {
        console.log("Fevereiro");
    }
    
    else if (mês == 3)
    {
        console.log("Março");
    }
    
    else if (mês == 4)
    {
        console.log("Abril");
    }
    
    else if (mês == 5)
    {
        console.log("Maio");
    }

    else if (mês == 6)
    {
        console.log("Junho");
    }

    else if (mês == 7)
    {
        console.log("Julho");
    }

    else if (mês == 8)
    {
        console.log("Agosto");
    }

    else if (mês == 9)
    {
        console.log("Setembro");
    }

    else if (mês == 10)
    {
        console.log("Outubro");
    }

    else if (mês == 11)
    {
        console.log("Novembro");
    }

    else if (mês == 12)
    {
        console.log("Dezembro");
    }
}