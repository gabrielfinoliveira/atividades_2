// A nota final de um estudante é calculada a partir de três notas atribuídas respectivamente a um trabalho de laboratório, a uma avaliação semestral e a um exame final. A média das três notas mencionadas anteriormente obedece aos pesos a seguir.

namespace atividade_rep
{
    let nota1, nota2, nota3, nota4, peso1, peso2, peso3: number;

    nota1 = 8
    nota2 = 4
    nota3 = 60
    peso1 = 20
    peso2 = 30
    peso3 = 50
    
    let media: number;
    media = (nota1*peso1 + nota2*peso2 + nota3+peso3) / (peso1 + peso2 + peso3);
    
    if(media >= 8 && media <= 10)
    {console.log("Sua nota é A");}

    if(media >= 7 && media < 8)
    {console.log("Sua nota é B");}

    if(media >= 6 && media < 7)
    {console.log("Sua nota é C")}

    if(media >= 5 && media < 6)
    {console.log("Sua nota é D")}

    if(media >= 0 && media < 5)
    {console.log("Sua nota é E")}

    

}